# KeyboardUno

A library for turning the Arduino Uno into a keyboard

To make the Arduino Uno into a keyboard, you have to reprogram its ATmega16U2.
And here's how to do it:

Download the file at: ﻿http://www.tinkernut.com/demos/arduino_hid/arduino_hid.zip﻿ and then unzip it.  The file we just downloaded contains the firmware we will need to send to the ATmega16u2.

Now, let's make the ATmega16u2 a keyboard.

For Windows:
​Download Atmel's flip programmer from http://www.atmel.com/tools/flip.aspx.
Install flip.
Press the thing that looks like an IC and select ATmega16u2.

Plug your Arduino into your computer with a female-to-female wire on the 2 ICSP pins closest to the USB port.

Once the Arduino boots up, unplug the female-to-female wire.
(you only have to do the below 7 lines once)
Open Device Manager
Look for "unknown device"
right click the entry and select "update driver"
select "search for driver on computer"
navigate to "C:\Program Files (x86)\Atmel\Flip 3.4.7\usb" or wherever you installed flip
confirm the directory
the computer will find the correct driver and install it
(above 7 lines copied from https://github.com/grbl/grbl/issues/995)
Open flip.
​Press Ctrl+U.
​Press "Open".
​Press Ctrl+L.
Remember when we downloaded the zip file before and unzipped it?  Go to the unzipped folder and select "Arduino-keyboard-0.3.hex".
​
​Now hit run and wait for flip to finish.
Power cycle the Arduino, and you have a working keyboard!

For Mac/Linux:

"Mac: Install MacPorts following the instructions at: http://www.macports.org/install.php#pkg
Once MacPorts is installed, in a Terminal window, type sudo port install dfu-programmer

NB: If you've never used sudo before, it will ask for your password. Use the password you login to your Mac with. sudo allows you to run commands as the administrator of the computer

Linux: from a command line type

sudo apt-get install dfu-programmer

or

sudo aptitude install dfu-programmer

depending on your distribution"

(above block of text copied from https://www.arduino.cc/en/Hacking/DFUProgramming8U2)

"From a terminal window, change directories to get into the folder with the firmware. If you saved the zip file in your downloads folder on OSX, then you might type
_cd Downloads/arduino_hid/_
Once there, type:
_sudo dfu-programmer atmega16u2 erase_
When this command is done and you get a command prompt again, type
_sudo dfu-programmer atmega16u2 flash Arduino-keyboard-0.3.hex_
Finally, type
_sudo dfu-programmer atmega16u2 reset_"

(the above block of text was copied from https://www.arduino.cc/en/Hacking/DFUProgramming8U2, but I modified it a little)


To turn your Arduino back into a regular Arduino, follow the same steps as above but replace "Arduino-keyboard-0.3.hex" with "Arduino-usbserial-uno.hex"
