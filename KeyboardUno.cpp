/*
KeyboardUno.cpp
This library lets the Arduino Uno turn into a keyboard
By sending USB HID keycodes
Copyright (c) 2017 Ben Goldberg
*/
#include "Arduino.h"
#include "KeyboardUno.h"
void releaseKey() // Release keys
{
  uint8_t buf[8] = {
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0
  };
  buf[0] = 0;
  buf[2] = 0;
  Serial.write(buf, 8);
}

KeyboardUno::KeyboardUno() { // constructor

}

void KeyboardUno::begin() { // starts USB communication
  Serial.begin(9600);
}

void KeyboardUno::print(String text) { // print a message
  buf[0] = 0;
  for (char i : text) {
    delay(5);
    if (_asciimap[i] & 0x80) { // is shift needed?
      buf[0] = B10; // If so, make the first digit of buf a shift (2)
      buf[2] = _asciimap[i] & 0x7F; // the third digit of buf is the keypress without shift
    }
    else { // no shift needed
      buf[0] = 0; // First digit of buf is 0, meaning no control or shift keys or whatever
      buf[2] = _asciimap[i]; // map ascii character to keypress for third digit of buf
    }
    Serial.write(buf, 8); // send buf (the keyboard data) over usb
    releaseKey();
  }
}

void KeyboardUno::println(String text) { // print a message with newline
  buf[0] = 0;
  for (char i : text) {
    delay(5);
    if (_asciimap[i] & 0x80) { // is shift needed?
      buf[0] = B10; // If so, make the first digit of buf a shift (2)
      buf[2] = _asciimap[i] & 0x7F; // the third digit of buf is the keypress without shift
    }
    else { // no shift needed
      buf[0] = 0; // First digit of buf is 0, meaning no control or shift keys or whatever
      buf[2] = _asciimap[i]; // map ascii character to keypress for third digit of buf
    }
    Serial.write(buf, 8); // send buf (the keyboard data) over usb
    buf[0] = 0;
    buf[2] = 0;
    Serial.write(buf, 8); // Release key
  }
  buf[0] = 0;
  buf[2] = _asciimap['\n'];
  Serial.write(buf, 8);
  releaseKey();
}

void KeyboardUno::press(unsigned char c) { // press an individual key
  if (c != KEY_UP_ARROW and c != KEY_DOWN_ARROW and c != KEY_LEFT_ARROW and c != KEY_RIGHT_ARROW and c != KEY_CTRL and c != KEY_SHIFT and c != KEY_ALT and c != KEY_BACKSPACE and c != KEY_COMMAND) {
    if (_asciimap[c] & 0x80) { // Does the keypress have shift?
      buf[0] = B10; // If so, make the first digit of buf a shift (2)
      buf[counter] = _asciimap[c] & 0x7F; // the third digit of buf is the keypress without shift
      counter++;
    }
    else {
      buf[counter] = _asciimap[c];
      counter++;
    }
  }
  else if (c == KEY_CTRL) {
    buf[counter] = 0xE0;
    counter++;
  }
  else if (c == KEY_SHIFT) {
    buf[counter] = 0xE1;
    counter++;
  }
  else if (c == KEY_BACKSPACE) {
    buf[counter] = 0x2A;
    counter++;
  }
  else if (c == KEY_ALT)  {
    buf[counter] = 0xE2;
    counter++;
  }
  else if (c == KEY_COMMAND) {
    buf[counter] = 0xE3;
    counter++;
  }
  else if (c == KEY_MUTE) {
    buf[counter] = 0x7F;
    counter++;
  }
  else if (c == KEY_VOLUME_UP) {
    buf[counter] = 0x80;
    counter++;
  }
  else if (c == KEY_VOLUME_DOWN) {
    buf[counter] = 0x81;
    counter++;
  }
  else if (c == KEY_UP_ARROW) {
    buf[counter] = 0x52;
    counter++;
  }
  else if (c == KEY_DOWN_ARROW) {
    buf[counter] = 0x51;
    counter++;
  }
  else if (c == KEY_LEFT_ARROW) {
    buf[counter] = 0x50;
    counter++;
  }
  else if (c == KEY_RIGHT_ARROW) {
    buf[counter] = 0x4F;
    counter++;
  }
}

void KeyboardUno::send() { // send individual keypresses (usually used at the end of a keyboard shortcut)
  uint8_t buf2[8] = {0, 0, 0, 0, 0, 0, 0, 0};
  for (int i = 0; i <= 7; i++) {
    buf2[i] = buf[i];
    Serial.write(buf2, 8);
  }
  releaseKey();
  counter = 2;
}

void KeyboardUno::pressFunctionKey(int f) { // press a function key
  buf[counter] = 0x3A+(f-1);
  counter++;
}
