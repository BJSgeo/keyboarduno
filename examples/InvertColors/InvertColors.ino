/*
   InvertColors.ino
   This program inverts a computer's colors. =D
   Copyright (c) 2017 Ben Goldberg
   MIT License (see LICENSE in this repository)
*/
#include <KeyboardUno.h>
KeyboardUno Keyboard;
#define WINDOWS 0
#define MAC 1
#define PLATFORM WINDOWS // If you have a Mac, comment this line out and uncomment the next line.
//#define PLATFORM MAC
void setup() {
  // put your setup code here, to run once:
  Keyboard.begin();
  delay(2500);
  if (PLATFORM == WINDOWS) { // Open Magnifier, then Ctrl-Alt-I
    Keyboard.press(KEY_WINDOWS);
    Keyboard.send();
    delay(500);
    Keyboard.println("Magnifier");
    delay(1000);
    Keyboard.press(KEY_CTRL);
    Keyboard.press(KEY_ALT);
    Keyboard.press('i');
    Keyboard.send();
  }
  if (PLATFORM == MAC) { // Ctrl-Option-Command-8
    Keyboard.press(KEY_CTRL);
    Keyboard.press(KEY_OPTION);
    Keyboard.press(KEY_COMMAND);
    Keyboard.press('8');
    Keyboard.send();
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
