/*
   OpenProgram.ino
   This program opens a program.
   Copyright (c) 2017 Ben Goldberg
   MIT License (see LICENSE in this repository)
*/
#include <KeyboardUno.h>
KeyboardUno Keyboard;
String programName = "Calculator";
#define WINDOWS 0
#define MAC 1
#define PLATFORM WINDOWS // Comment this out and uncomment the next line if you have a Mac
//#define PLATFORM MAC
void setup() {
  // put your setup code here, to run once:
  Keyboard.begin();
  delay(2500);
  if (PLATFORM == WINDOWS) { // Windows key to launch program
    Keyboard.press(KEY_WINDOWS);
    Keyboard.send();
    delay(1000);
    Keyboard.println(programName);
  }
  if (PLATFORM == MAC) { // Command-Space to launch program
    Keyboard.press(KEY_COMMAND);
    Keyboard.press(' ');
    Keyboard.send();
    delay(1000);
    Keyboard.println(programName);
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
