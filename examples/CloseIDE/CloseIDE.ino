/*
   CloseIDE.ino
   This program closes the Arduino IDE if you have it open.
   Copyright (c) 2017 Ben Goldberg
   MIT License (see LICENSE in this repository)
*/
#define OSX 0
#define WINDOWS_LINUX 1
//if you have Mac OSX, comment this out and uncomment the line after it
#define PLATFORM WINDOWS_LINUX
//#define PLATFORM OSX
#include <KeyboardUno.h>
KeyboardUno Keyboard;
void setup() {
  // put your setup code here, to run once:
  Keyboard.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(2500);
  if (PLATFORM == WINDOWS_LINUX) {
    Keyboard.press(KEY_CTRL);
    Keyboard.press('w');
    Keyboard.send();
    delay(1000);
    while (1);
  }
  if (PLATFORM == OSX) {
    Keyboard.press(KEY_COMMAND);
    Keyboard.press('w');
    Keyboard.send();
    delay(1000);
    while (1);
  }
}
