/*
   MinimizeApp.ino
   This program minimizes the current app you have open.
   Copyright (c) 2017 Ben Goldberg
   MIT License (see LICENSE in this repository)
*/
#include <KeyboardUno.h>
KeyboardUno Keyboard;
#define WINDOWS 0
#define MAC 1
#define PLATFORM WINDOWS // If you have a Mac, comment this line out and uncomment the next line.
//#define PLATFORM MAC
void setup() {
  // put your setup code here, to run once:
  Keyboard.begin();
  delay(2500);
  if (PLATFORM == WINDOWS) { // Windows-Down Arrow
    Keyboard.press(KEY_WINDOWS);
    Keyboard.press(KEY_DOWN_ARROW);
    Keyboard.send();
  }
  if (PLATFORM == MAC) { // Command-M
    Keyboard.press(KEY_COMMAND);
    Keyboard.press('m');
    Keyboard.send();
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
