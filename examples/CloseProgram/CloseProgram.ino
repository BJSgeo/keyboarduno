/*
   CloseProgram.ino
   This program closes the current program you have open.
   Copyright (c) 2017 Ben Goldberg
   MIT License (see LICENSE in this repository)
*/
#include <KeyboardUno.h>
KeyboardUno Keyboard;
#define WINDOWS_LINUX 0
#define OSX 1
#define PLATFORM WINDOWS_LINUX // If you have a Mac, comment this out and uncomment the next line
//#define PLATFORM OSX
void setup() {
  // put your setup code here, to run once:
  Keyboard.begin();
  if (PLATFORM == WINDOWS_LINUX) { // Alt-F4
    Keyboard.press(KEY_ALT);
    Keyboard.pressFunctionKey(4);
    Keyboard.send();
  }
  else if (PLATFORM == OSX) { // Command-Q
    Keyboard.press(KEY_COMMAND);
    Keyboard.press('q');
    Keyboard.send();
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
