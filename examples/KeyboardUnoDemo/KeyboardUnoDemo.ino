/*
   KeyboardUnoDemo.ino
   This program types a message over and over.
   Copyright (c) 2017 Ben Goldberg
   MIT License (see LICENSE in this repository)
*/
#include <KeyboardUno.h>
KeyboardUno Keyboard;
void setup() {
  // put your setup code here, to run once:
  Keyboard.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(1000);
  Keyboard.println("Hi from the Arduino that learned to type.");
}
